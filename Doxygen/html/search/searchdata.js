var indexSectionsWithContent =
{
  0: "abcdeghilmnoprstuwx",
  1: "abegilmp",
  2: "bx",
  3: "abcegilmoprsw",
  4: "b",
  5: "nrs",
  6: "bcdeghimpstuw",
  7: "g"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Events"
};

