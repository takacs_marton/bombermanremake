﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static MainWindow mW;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// maga a fő ablak
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.Background = Load.LoadSkin("ground.jpg");
            mW = this;
        }

        /// <summary>
        /// singleton mainwindow... elég csak visszadni hisz a program inditásakor elve lére jön egy
        /// </summary>
        /// <returns>a mainwindow-ot</returns>
        public static MainWindow Get()
        {
            return mW;
        }

        /// <summary>
        /// megnyitja a szabad pályát egy játékosra
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">click</param>
        private void Btn_SandBox_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            GameWindow gw = new GameWindow(0);
            gw.Show();
        }

        /// <summary>
        /// elindítja a két player módot
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">click</param>
        private void Btn_twoPlayer_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            GameWindow gw = new GameWindow(1);
            gw.Show();
        }

        /// <summary>
        /// megnyitja a help ablakot
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">click</param>
        private void Btn_help_Click(object sender, RoutedEventArgs e)
        {
            Instructions inst = new Instructions();
            inst.Show();
        }
    }
}
