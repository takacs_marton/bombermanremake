﻿// <copyright file="HelpFunctions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// néhány segéd függvény
    /// </summary>
    public static class HelpFunctions
    {
        /// <summary>
        /// adott pontot kerekít egy olyan értékre amin létezik elem a pályán
        /// </summary>
        /// <param name="kerek">ezt a ontot</param>
        /// <returns>ilyenné</returns>
        public static Point Kerekit(Point kerek)
        {
            Point tmp = default(Point);
            if (kerek.X % 40 < 21)
            {
                tmp.X = kerek.X - (kerek.X % 40);
            }
            else
            {
                tmp.X = kerek.X + (40 - (kerek.X % 40));
            }

            if (kerek.Y % 40 < 21)
            {
                tmp.Y = kerek.Y - (kerek.Y % 40);
            }
            else
            {
                tmp.Y = kerek.Y + (40 - (kerek.Y % 40));
            }

            return tmp;
        }

        /// <summary>
        /// megnézi hogy két geomety ütközött e
        /// </summary>
        /// <param name="ami"> ez </param>
        /// <param name="amivel">ezzel</param>
        /// <returns>true akkor ütközött</returns>
        public static bool Utkozik(Geometry ami, Geometry amivel)
        {
            Geometry eredmény = Geometry.Combine(ami, amivel, GeometryCombineMode.Intersect, null);
            return eredmény.GetArea() > 0;
        }
    }
}
