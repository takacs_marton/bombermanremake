﻿// <copyright file="Life.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// A player élete
    /// </summary>
    public class Life
    {
        private List<Element> hearths;

        /// <summary>
        /// Initializes a new instance of the <see cref="Life"/> class.
        /// a player helyzete alapján meghatáározza hogy kié az élet
        /// </summary>
        /// <param name="helyzet">a player kezdő helye</param>
        public Life(Point helyzet)
        {
            this.hearths = new List<Element>();
            if (helyzet.X < 50)
            {
                this.hearths.Add(new Element(new Point(0, 0), "hearth.jpg", false));
                this.hearths.Add(new Element(new Point(40, 0), "hearth.jpg", false));
                this.hearths.Add(new Element(new Point(80, 0), "hearth.jpg", false));
            }
            else
            {
                this.hearths.Add(new Element(new Point(560, 480), "hearth.jpg", false));
                this.hearths.Add(new Element(new Point(520, 480), "hearth.jpg", false));
                this.hearths.Add(new Element(new Point(480, 480), "hearth.jpg", false));
            }
        }

        /// <summary>
        /// Gets or sets szivek
        /// </summary>
        public List<Element> Hearths
        {
            get
            {
                return this.hearths;
            }

            set
            {
                this.hearths = value;
            }
        }
    }
}
