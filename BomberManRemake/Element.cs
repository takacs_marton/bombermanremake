﻿// <copyright file="Element.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// játék elem osztály
    /// </summary>
    public class Element
    {
        private static Size m;
        private Point helyzet;
        private ImageBrush skin;
        private bool isDestructable;

        /// <summary>
        /// Initializes a new instance of the <see cref="Element"/> class.
        /// létre hozza a játékelemet
        /// </summary>
        /// <param name="helyzet">ide</param>
        /// <param name="skin">igy nézze ki</param>
        /// <param name="isDestructable">elpusztítthtó e</param>
        /// <param name="isplayer">player e</param>
        public Element(Point helyzet, string skin, bool isDestructable, bool isplayer = false)
        {
            this.helyzet = helyzet;
            this.skin = Load.LoadSkin(skin, isplayer);
            this.isDestructable = isDestructable;
        }

        /// <summary>
        /// Gets size (visszadaja a méretét)
        /// </summary>
        public static Size GetsM
        {
            get
            {
                m.Height = 40;
                m.Width = 40;
                return m;
            }
        }

        /// <summary>
        /// Gets or sets helyzet
        /// </summary>
        public Point Helyzet
        {
            get
            {
                return this.helyzet;
            }

            set
            {
                this.helyzet = value;
            }
        }

        /// <summary>
        /// Gets this imagebrush
        /// </summary>
        public ImageBrush Skin
        {
            get
            {
                return this.skin;
            }
        }

        /// <summary>
        /// Gets a value indicating whether isdestructable
        /// </summary>
        public bool IsDestructable
        {
            get
            {
                return this.isDestructable;
            }
        }

        /// <summary>
        /// Gets geometry
        /// </summary>
        public virtual Geometry G
        {
            get { return new RectangleGeometry(new Rect(this.helyzet, Element.GetsM)); }
        }
    }
}
