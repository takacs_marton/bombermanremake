﻿// <copyright file="Bomb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// A bomba amit a player letesz
    /// </summary>
    public class Bomb : Element
    {
        private bool isExploded;
        private List<Element> effect;
        private DispatcherTimer dt = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(4) };
        private DispatcherTimer dtVisual = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(3) };

        /// <summary>
        /// Initializes a new instance of the <see cref="Bomb"/> class.
        /// létrehozza az adott bombát
        /// </summary>
        /// <param name="bombrange">ekkorát fog robbani</param>
        /// <param name="terkep">kell a térképen elhelyezkedő elemek miatt </param>
        /// <param name="helyzet"> hova</param>
        /// <param name="skin">a kép</param>
        /// <param name="isDestructable">elpusztíható e</param>
        /// <param name="isplayer">default nem player</param>
        public Bomb(int bombrange, Map terkep, Point helyzet, string skin, bool isDestructable, bool isplayer = false)
            : base(helyzet, skin, isDestructable, isplayer)
        {
            this.isExploded = false;
            this.dt.Tick += this.Exp_Tick;
            this.dt.Start();
            this.dtVisual.Tick += this.DtVisual_Tick;
            this.dtVisual.Start();
            this.effect = new List<Element>();
            this.Bombrage = bombrange;
            this.Terkep = terkep;
        }

        /// <summary>
        /// Gets a value indicating whether isExploded
        /// </summary>
        public bool IsExploded
        {
            get
            {
                return this.isExploded;
            }
        }

        /// <summary>
        /// Gets or sets  bomba robbánás sugár
        /// </summary>
        public int Bombrage { get; set; }

        /// <summary>
        /// Gets or sets térkép
        /// </summary>
        public Map Terkep { get; set; }

        /// <summary>
        /// Gets or sets robbanás effekt
        /// </summary>
        public List<Element> Effect
        {
            get
            {
                return this.effect;
            }

            set
            {
                this.effect = value;
            }
        }

        /// <summary>
        /// mikor csinája meg a visualt
        /// </summary>
        /// <param name="sender">dtvisual</param>
        /// <param name="e">tick</param>
        private void DtVisual_Tick(object sender, EventArgs e)
        {
            this.dtVisual.Start();
            this.CreateBombVisual(this.Bombrage, this.Terkep);
        }

        /// <summary>
        /// mikor tünjön el a bomba az effektel együtt
        /// </summary>
        /// <param name="sender">dt</param>
        /// <param name="e">tick</param>
        private void Exp_Tick(object sender, EventArgs e)
        {
            this.dt.Stop();
            this.dtVisual.Stop();
            this.isExploded = true;
        }

        /// <summary>
        /// megcsinálja a bomba robbanás efektjét
        /// </summary>
        /// <param name="bombrange">ekkora rangebe</param>
        /// <param name="terkep">kell hogy meddig mehet a robbanás</param>
        private void CreateBombVisual(int bombrange, Map terkep)
        {
            this.effect.Add(new Element(this.Helyzet, "explode_center.jpg", false));
            List<Element> removableBarrels = new List<Element>();

            int k = 1;
            while (k <= bombrange && (terkep.WhatIsHere(new Point(this.Helyzet.X + (k * 40), this.Helyzet.Y)) == null || terkep.WhatIsHere(new Point(this.Helyzet.X + (k * 40), this.Helyzet.Y)).IsDestructable))
            {
                this.effect.Add(new Element(new Point(this.Helyzet.X + (k * 40), this.Helyzet.Y), "explode_x.jpg", false));
                Element dyingBarrel = terkep.WhatIsHere(new Point(this.Helyzet.X + (k * 40), this.Helyzet.Y));
                if (dyingBarrel != null && dyingBarrel.IsDestructable == true)
                {
                    removableBarrels.Add(dyingBarrel);
                    k = 10;
                }

                k++;
            }

            k = 1;
            while (k <= bombrange && (terkep.WhatIsHere(new Point(this.Helyzet.X - (k * 40), this.Helyzet.Y)) == null || terkep.WhatIsHere(new Point(this.Helyzet.X - (k * 40), this.Helyzet.Y)).IsDestructable))
            {
                this.effect.Add(new Element(new Point(this.Helyzet.X - (k * 40), this.Helyzet.Y), "explode_x.jpg", false));
                Element dyingBarrel = terkep.WhatIsHere(new Point(this.Helyzet.X - (k * 40), this.Helyzet.Y));
                if (dyingBarrel != null && dyingBarrel.IsDestructable == true)
                {
                    removableBarrels.Add(dyingBarrel);
                    k = 10;
                }

                k++;
            }

            k = 1;
            while (k <= bombrange && (terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y + (k * 40))) == null || terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y + (k * 40))).IsDestructable))
            {
                this.effect.Add(new Element(new Point(this.Helyzet.X, this.Helyzet.Y + (k * 40)), "explode_y.jpg", false));
                Element dyingBarrel = terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y + (k * 40)));
                if (dyingBarrel != null && dyingBarrel.IsDestructable == true)
                {
                    removableBarrels.Add(dyingBarrel);
                    k = 10;
                }

                k++;
            }

            k = 1;
            while (k <= bombrange && (terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y - (k * 40))) == null || terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y - (k * 40))).IsDestructable))
            {
                this.effect.Add(new Element(new Point(this.Helyzet.X, this.Helyzet.Y - (k * 40)), "explode_y.jpg", false));
                Element dyingBarrel = terkep.WhatIsHere(new Point(this.Helyzet.X, this.Helyzet.Y - (k * 40)));
                if (dyingBarrel != null && dyingBarrel.IsDestructable == true)
                {
                    removableBarrels.Add(dyingBarrel);
                    k = 10;
                }

                k++;
            }

            terkep.RemoveBarrels(removableBarrels);
        }
    }
}
