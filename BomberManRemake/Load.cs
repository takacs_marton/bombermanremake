﻿// <copyright file="Load.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// static class for loading emblemed resource files
    /// </summary>
    public static class Load
    {
        /// <summary>
        /// Vissza adja egy elem texturáját
        /// </summary>
        /// <param name="path"> melyik képet akarjuk elérni</param>
        /// <param name="player">az adott kép playerhez kötödik e</param>
        /// <returns>vissza adja a kéet image brushként</returns>
        public static ImageBrush LoadSkin(string path, bool player = false)
        {
            Assembly assembly;
            Stream imageStream;
            assembly = Assembly.GetExecutingAssembly();
            imageStream = assembly.GetManifestResourceStream("BomberManRemake.Imgs." + path);
            BitmapImage pic = new BitmapImage();
            pic.BeginInit();
            pic.StreamSource = imageStream;
            pic.CacheOption = BitmapCacheOption.OnLoad;
            pic.EndInit();
            pic.Freeze();
            ImageBrush ib = new ImageBrush(pic);
            if (!player)
            {
                ib.TileMode = TileMode.Tile;
                ib.Viewport = new Rect(0, 0, 40, 40);
                ib.ViewportUnits = BrushMappingMode.Absolute;
            }

            return ib;
        }

        /// <summary>
        /// Betölti a pályát
        /// </summary>
        /// <returns> visszadja a pályát egy string tömbként</returns>
        public static string[] LoadMap()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            var resourceName = "BomberManRemake.map.txt";

            string result;
            Stream stream = assembly.GetManifestResourceStream(resourceName);
            StreamReader reader = new StreamReader(stream);
            result = reader.ReadToEnd();

            return result.Split(',');
        }
    }
}
