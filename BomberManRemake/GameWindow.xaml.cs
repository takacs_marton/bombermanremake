﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private int gameTye;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// maga a játék ablak
        /// </summary>
        /// <param name="gametype">0=sandbox, 1=two player</param>
        public GameWindow(int gametype)
        {
            this.InitializeComponent();
            this.Background = Load.LoadSkin("ground.jpg");
            this.gameTye = gametype;
        }

        /// <summary>
        /// Gets or sets gametype
        /// </summary>
        public int GameTye
        {
            get
            {
                return this.gameTye;
            }

            set
            {
                this.gameTye = value;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.Get().Show();
        }
    }
}
