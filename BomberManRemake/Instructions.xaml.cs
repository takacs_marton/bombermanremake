﻿// <copyright file="Instructions.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Instructions.xaml
    /// ebben az ablakban jelenik meg a játék utmutató
    /// </summary>
    public partial class Instructions : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Instructions"/> class.
        /// help kép beálítása
        /// </summary>
        public Instructions()
        {
            this.InitializeComponent();
            this.Background = Load.LoadSkin("controlls.jpg", true);
        }
    }
}
