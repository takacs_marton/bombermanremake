﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// maga a player
    /// </summary>
    public class Player : Element
    {
        private DispatcherTimer dtHp;
        private DispatcherTimer dtUtkozott;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// létre hozza az adott playert
        /// </summary>
        /// <param name="helyzet">itt</param>
        /// <param name="skin">ezzel a képpel</param>
        /// <param name="isDestructable">nem elpusztítható</param>
        /// <param name="isplayer">igaz</param>
        public Player(Point helyzet, string skin, bool isDestructable, bool isplayer)
            : base(helyzet, skin, isDestructable, isplayer)
        {
            this.Speed = 4;
            this.MaxBombNumber = 1;
            this.BombRange = 1;
            this.HP = new Life(helyzet);
            this.Tnt = new List<Bomb>();

            this.dtHp = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(30) };
            this.dtHp.Tick += this.DtHp_Tick;
            this.dtHp.Start();

            this.dtUtkozott = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(3) };
            this.dtUtkozott.Tick += this.DtUtkozott_Tick;
        }

        /// <summary>
        /// Gets dtutközött
        /// </summary>
        public DispatcherTimer DtUtkozott
        {
            get
            {
                return this.dtUtkozott;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets Ütközött e
        /// </summary>
        public bool Utkozott { get; set; }

        /// <summary>
        /// Gets or sets Tnt
        /// </summary>
        public List<Bomb> Tnt { get; set; }

        /// <summary>
        /// Gets or sets current bumb number
        /// </summary>
        public int CurrentBombNumber { get; set; }

        /// <summary>
        /// Gets or sets MaxBombNumber
        /// </summary>
        public int MaxBombNumber { get; set; }

        /// <summary>
        /// Gets or sets speed
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Gets or sets Bombrange
        /// </summary>
        public int BombRange { get; set; }

        /// <summary>
        /// Gets or sets Hp
        /// </summary>
        public Life HP { get; set; }

        /// <summary>
        /// Gets geometry of this
        /// </summary>
        public override Geometry G
        {
            get
            {
                return new RectangleGeometry(new Rect(this.Helyzet, new Size(Element.GetsM.Height - 4, Element.GetsM.Width - 4)));
            }
        }

        /// <summary>
        /// a játékos mozgatása a térképen
        /// </summary>
        /// <param name="dx">dx irányba</param>
        /// <param name="dy">dy riányba</param>
        /// <param name="terkep"> itt</param>
        public void Mozog(int dx, int dy, Map terkep)
        {
            Point oldHelyzet = this.Helyzet;
            double ujX = this.Helyzet.X + dx;
            double ujY = this.Helyzet.Y + dy;
            Point ujpoint = new Point(ujX, ujY);
            RectangleGeometry uj = new RectangleGeometry(new Rect(ujpoint, new Point(ujX + Element.GetsM.Width - 4, ujY + Element.GetsM.Height - 4)));
            if (!HelpFunctions.Utkozik(uj, terkep.AllElemGeo()))
            {
                this.Helyzet = ujpoint;
                if (HelpFunctions.Utkozik(uj, terkep.AllPowerUps())) //// bonusszak ütközik
                {
                    if (dx > 0)
                    {
                        if (this.Speed != 20)
                        {
                        ujpoint.X += 40;
                        }
                    }

                    if (dx < 0)
                    {
                        ujpoint.X -= 40;
                    }

                    if (dy > 0)
                    {
                        if (this.Speed != 20)
                        {
                        ujpoint.Y += 40;
                        }
                    }

                    if (dy < 0)
                    {
                        ujpoint.Y -= 40;
                    }

                    PowerUps ezt = terkep.GetsPwu(HelpFunctions.Kerekit(ujpoint));
                    if (ezt != null)
                    {
                        switch (ezt.Extra)
                        {
                            case Bonus.Speed:
                                this.Helyzet = oldHelyzet;
                                if (this.Speed == 4)
                                {
                                    this.Speed = 10;
                                    break;
                                }

                                if (this.Speed == 10)
                                {
                                    this.Speed = 20;
                                    break;
                                }

                                break;
                            case Bonus.Range:
                                if (this.BombRange < 3)
                                {
                                    this.BombRange++;
                                }

                                break;
                            case Bonus.Number:
                                if (this.MaxBombNumber < 3)
                                {
                                    this.MaxBombNumber++;
                                }

                                break;
                            default:
                                break;
                        }
                    }

                    terkep.Pwu.Remove(ezt);
                }
            }
        }

        /// <summary>
        /// a space hatására letessi a bombát
        /// </summary>
        /// <param name="terkep"> kell hogy hova tudja tenni a robanás efektjét</param>
        public void BombPlace(Map terkep)
        {
            if (this.Tnt.Count < this.MaxBombNumber && !this.IsThereBomb(HelpFunctions.Kerekit(this.Helyzet)))
            {
                this.Tnt.Add(new Bomb(this.BombRange, terkep, HelpFunctions.Kerekit(this.Helyzet), "bomb.jpg", false));
            }
        }

        /// <summary>
        /// figyeli a felrobban bombákat amit majd a game-be a ticker kiszed
        /// </summary>
        public void InaktivBombRemove()
        {
            int i = 0;
            while (i < this.Tnt.Count)
            {
                if (this.Tnt[i].IsExploded)
                {
                    this.Tnt.RemoveAt(i);
                    i = 0;
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// sérülés után 3 másodpercig nem tud sérülni a játékos
        /// </summary>
        /// <param name="sender">timer</param>
        /// <param name="e">event</param>
        private void DtUtkozott_Tick(object sender, EventArgs e)
        {
            this.dtUtkozott.Stop();
            this.Utkozott = false;
        }

        /// <summary>
        /// ellenörzi hogy belemente a saját bombájába
        /// </summary>
        /// <param name="sender">timer</param>
        /// <param name="e">event</param>
        private void DtHp_Tick(object sender, EventArgs e)
        {
            foreach (Bomb b in this.Tnt)
            {
                foreach (Element i in b.Effect)
                {
                    if (!this.Utkozott && HelpFunctions.Utkozik(this.G, i.G))
                    {
                        if (this.HP.Hearths.Count != 0)
                        {
                            this.HP.Hearths.RemoveAt(this.HP.Hearths.Count - 1);
                            this.Utkozott = true;
                            this.dtUtkozott.Start();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// megnézi hogy az adott helyen van e már bomba
        /// </summary>
        /// <param name="here">itt</param>
        /// <returns>van/nincs</returns>
        private bool IsThereBomb(Point here)
        {
            foreach (Bomb b in this.Tnt)
            {
                if (b.Helyzet == here)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
