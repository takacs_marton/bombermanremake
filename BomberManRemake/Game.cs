﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// ez az osztály kezeli az egész játékot
    /// </summary>
    public class Game : FrameworkElement
    {
        private Player blue;
        private Player red;
        private Map terkep;
        private int gameMode;
        private DispatcherTimer dt;
        private DispatcherTimer dtkeyread;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// létrehozza a játékot kezekő osztályt
        /// </summary>
        public Game()
        {
            this.red = new Player(new Point(Element.GetsM.Height + 2, Element.GetsM.Width + 2), "red_player.jpg", true, true);
            this.terkep = new Map();
            this.Loaded += this.Game_Loaded;
        }

        /// <summary>
        /// játék vége esemény
        /// </summary>
        public event EventHandler GameOver;

        /// <summary>
        /// amiket renderelni kell
        /// </summary>
        /// <param name="drawingContext">d</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            foreach (Element g in this.terkep.Walls)
            {
                drawingContext.DrawGeometry(g.Skin, new Pen(), g.G);
            }

            foreach (Element h in this.red.HP.Hearths)
            {
                drawingContext.DrawGeometry(h.Skin, new Pen(), h.G);
            }

            foreach (Element g in this.terkep.Barrels)
            {
                drawingContext.DrawGeometry(g.Skin, new Pen(), g.G);
            }

            foreach (PowerUps p in this.terkep.Pwu)
            {
                drawingContext.DrawGeometry(p.Skin, new Pen(), p.G);
            }

            foreach (Bomb b in this.red.Tnt)
            {
                drawingContext.DrawGeometry(b.Skin, new Pen(), b.G);

                foreach (Element v in b.Effect)
                {
                    drawingContext.DrawGeometry(v.Skin, new Pen(), v.G);
                }
            }

            drawingContext.DrawGeometry(this.red.Skin, new Pen(), this.red.G);
            if (this.gameMode == 1)
            {
                foreach (Bomb b in this.blue.Tnt)
                {
                    drawingContext.DrawGeometry(b.Skin, new Pen(), b.G);

                    foreach (Element v in b.Effect)
                    {
                        drawingContext.DrawGeometry(v.Skin, new Pen(), v.G);
                    }
                }

                foreach (Element h in this.blue.HP.Hearths)
                {
                    drawingContext.DrawGeometry(h.Skin, new Pen(), h.G);
                }

                drawingContext.DrawGeometry(this.blue.Skin, new Pen(), this.blue.G);
            }
        }

        /// <summary>
        /// betoltéskor az adott módhoz szükséges dolgokat álítja be
        /// </summary>
        /// <param name="sender">loaded</param>
        /// <param name="e">e</param>
        private void Game_Loaded(object sender, RoutedEventArgs e)
        {
            this.gameMode = ((GameWindow)this.Parent).GameTye;
            if (this.gameMode == 1)
            {
                this.blue = new Player(new Point(522, 442), "blue_player.jpg", true, true);
                this.GameOver += this.Game_GameOver;

                this.dtkeyread = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(1) };
                this.dtkeyread.Tick += this.Dtkeyread_Tick;
                this.dtkeyread.Start();
            }

            this.dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(20) };
            this.dt.Tick += this.Dt_Tick;
            this.dt.Start();
        }

        /// <summary>
        /// figyeli a kettes player által lenyomott gombokat
        /// </summary>
        /// <param name="sender">dt keyread</param>
        /// <param name="e">dt</param>
        private void Dtkeyread_Tick(object sender, EventArgs e)
        {
            if (this.gameMode == 1)
            {
                if (Keyboard.IsKeyDown(Key.D))
                {
                    this.blue.Mozog(this.blue.Speed, 0, this.terkep);
                }

                if (Keyboard.IsKeyDown(Key.A))
                {
                    this.blue.Mozog(-this.blue.Speed, 0, this.terkep);
                }

                if (Keyboard.IsKeyDown(Key.W))
                {
                    this.blue.Mozog(0, -this.blue.Speed, this.terkep);
                }

                if (Keyboard.IsKeyDown(Key.S))
                {
                    this.blue.Mozog(0, this.blue.Speed, this.terkep);
                }

                if (Keyboard.IsKeyDown(Key.G))
                {
                    this.blue.BombPlace(this.terkep);
                }

                this.blue.InaktivBombRemove();
                this.EnemiDamageCheck();
                if (this.red.HP.Hearths.Count == 0 || this.blue.HP.Hearths.Count == 0)
                {
                    this.InvalidateVisual();
                    this.GameOver(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// leálítja az dispacher timereket és kiírja a játék végét
        /// </summary>
        /// <param name="sender">event</param>
        /// <param name="e">event args</param>
        private void Game_GameOver(object sender, EventArgs e)
        {
            this.dt.Stop();
            this.dtkeyread.Stop();
            if (this.red.HP.Hearths.Count > 0)
            {
            MessageBox.Show("Game Over\nA Piros játékos győzött");
            }
            else
            {
                MessageBox.Show("Game Over\nA Kék játékos győzött");
            }
        }

        /// <summary>
        /// a timer alapján nézi a gomblenyomás és frissíti a képernyőt
        /// </summary>
        /// <param name="sender">timer</param>
        /// <param name="e">e</param>
        private void Dt_Tick(object sender, EventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Right))
            {
                this.red.Mozog(this.red.Speed, 0, this.terkep);
            }

            if (Keyboard.IsKeyDown(Key.Left))
            {
                this.red.Mozog(-this.red.Speed, 0, this.terkep);
            }

            if (Keyboard.IsKeyDown(Key.Up))
            {
                this.red.Mozog(0, -this.red.Speed, this.terkep);
            }

            if (Keyboard.IsKeyDown(Key.Down))
            {
                this.red.Mozog(0, this.red.Speed, this.terkep);
            }

            if (Keyboard.IsKeyDown(Key.Space))
            {
                this.red.BombPlace(this.terkep);
            }

            this.red.InaktivBombRemove();
            this.InvalidateVisual();
        }

        /// <summary>
        /// ellenörzi hogy nem e ment bele valaki a másik robbanásába
        /// </summary>
        private void EnemiDamageCheck()
        {
            // red with blue
            foreach (Bomb b in this.blue.Tnt)
            {
                foreach (Element i in b.Effect)
                {
                    if (!this.red.Utkozott && HelpFunctions.Utkozik(this.red.G, i.G))
                    {
                        if (this.red.HP.Hearths.Count != 0)
                        {
                            this.red.HP.Hearths.RemoveAt(this.red.HP.Hearths.Count - 1);
                            this.red.Utkozott = true;
                            this.red.DtUtkozott.Start();
                        }
                    }
                }
            }

            // blue with red
            foreach (Bomb b in this.red.Tnt)
            {
                foreach (Element i in b.Effect)
                {
                    if (!this.blue.Utkozott && HelpFunctions.Utkozik(this.blue.G, i.G))
                    {
                        if (this.blue.HP.Hearths.Count != 0)
                        {
                            this.blue.HP.Hearths.RemoveAt(this.blue.HP.Hearths.Count - 1);
                            this.blue.Utkozott = true;
                            this.blue.DtUtkozott.Start();
                        }
                    }
                }
            }
        }
    }
}
