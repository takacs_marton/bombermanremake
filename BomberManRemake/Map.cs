﻿// <copyright file="Map.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// A poweruppok fajtái
    /// </summary>
    public enum Bonus
    {
        /// <summary>
        /// speed a gyorsaság
        /// </summary>
        Speed,

        /// <summary>
        /// range a bomba robanási sugara
        /// </summary>
        Range,

        /// <summary>
        /// Number a bomba száma
        /// </summary>
        Number
    }

    /// <summary>
    /// A pálya kinézetért felel
    /// </summary>
    public class Map
    {
        private List<Element> walls;
        private List<Element> barrels;
        private List<PowerUps> pwu;

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class.
        /// betölti a txtből a mappot ahol 1 van ott fal ahol 0 oda sorsol hordót
        /// </summary>
        public Map()
        {
            this.pwu = new List<PowerUps>();
            this.walls = new List<Element>();
            this.barrels = new List<Element>();
            Random rnd = new Random();
            string[] osszesSor = Load.LoadMap();
            for (int i = 0; i < osszesSor.Length; i++)
            {
                for (int j = 0; j < osszesSor[i].Length; j++)
                {
                    char c = osszesSor[i][j];
                    if (c == '1')
                    {
                        Element tmp = new Element(new Point(j * Element.GetsM.Height, i * Element.GetsM.Width), "wall.jpg", false);
                        this.walls.Add(tmp);
                    }

                    if (c == '0' && rnd.Next(0, 100) < 85)
                    {
                        Element tmp = new Element(new Point(j * Element.GetsM.Height, i * Element.GetsM.Width), "barrel.jpg", true);
                        this.barrels.Add(tmp);
                    }
                }
            }
        }

        /// <summary>
        /// Gets wallls
        /// </summary>
        public List<Element> Walls
        {
            get
            {
                return this.walls;
            }
        }

        /// <summary>
        /// Gets barrels
        /// </summary>
        public List<Element> Barrels
        {
            get
            {
                return this.barrels;
            }
        }

        /// <summary>
        /// Gets or sets powerups
        /// </summary>
        public List<PowerUps> Pwu
        {
            get
            {
                return this.pwu;
            }

            set
            {
                this.pwu = value;
            }
        }

        /// <summary>
        /// A térképen lévő falakből és hordókból egy geometryt csinál
        /// </summary>
        /// <returns>geometry</returns>
        public Geometry AllElemGeo()
        {
            GeometryGroup tmp = new GeometryGroup();
            foreach (Element e in this.walls)
            {
                tmp.Children.Add(e.G);
            }

            foreach (Element e in this.barrels)
            {
                tmp.Children.Add(e.G);
            }

            return tmp.GetWidenedPathGeometry(new Pen());
        }

        /// <summary>
        /// miden pályán lévő power upból egy geometrit csinál
        /// </summary>
        /// <returns> pwu geometry</returns>
        public Geometry AllPowerUps()
        {
            GeometryGroup tmp = new GeometryGroup();
            foreach (PowerUps e in this.pwu)
            {
                tmp.Children.Add(e.G);
            }

            return tmp.GetWidenedPathGeometry(new Pen());
        }

        /// <summary>
        /// az adot ponton lévő elemet visszadja
        /// </summary>
        /// <param name="here">ezen a ponton</param>
        /// <returns>ami itt van</returns>
        public Element WhatIsHere(Point here)
        {
            foreach (Element i in this.walls)
            {
                if (i.Helyzet == here)
                {
                    return i;
                }
            }

            foreach (Element i in this.barrels)
            {
                if (i.Helyzet == here)
                {
                    return i;
                }
            }

            return null;
        }

        /// <summary>
        /// a listában átadott felrobbantott hordókat kiveszi a pályáról
        /// </summary>
        /// <param name="ezeket">ezeket</param>
        public void RemoveBarrels(List<Element> ezeket)
        {
            foreach (Element item in ezeket)
            {
                this.barrels.Remove(item);
                this.CreateBonus(item.Helyzet);
            }
        }

        /// <summary>
        /// hordo halálakor hívodik meg és letesz az adott helyre egy random bónuszt
        /// </summary>
        /// <param name="hol"> ide</param>
        public void CreateBonus(Point hol)
        {
            Random rnd = new Random();
            System.Threading.Thread.Sleep(20);
            int tmp = rnd.Next(0, 10);
            PowerUps ez;
            switch (tmp)
            {
                case 0:
                    ez = new PowerUps(Bonus.Number, hol, "Extratnt.jpg", true);
                    this.pwu.Add(ez);
                    break;
                case 1:
                    ez = new PowerUps(Bonus.Range, hol, "Lightning.jpg", true);
                    this.pwu.Add(ez);
                    break;
                case 2:
                    ez = new PowerUps(Bonus.Speed, hol, "Speed.jpg", true);
                    this.pwu.Add(ez);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// visszadja az adott helyen lévő power uppot ha van
        /// </summary>
        /// <param name="itt">itt</param>
        /// <returns>amu itt van</returns>
        public PowerUps GetsPwu(Point itt)
        {
            foreach (PowerUps i in this.pwu)
            {
                if (i.Helyzet == itt)
                {
                    return i;
                }
            }

            return null;
        }
    }
}
