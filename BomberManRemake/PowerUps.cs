﻿// <copyright file="PowerUps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BomberManRemake
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// A power uppokat létrehozó osztály
    /// </summary>
    public class PowerUps : Element
    {
        private Bonus extra;

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUps"/> class.
        /// létrehozza a power uppot
        /// </summary>
        /// <param name="extra">milyet</param>
        /// <param name="helyzet">hova</param>
        /// <param name="skin">kinézet</param>
        /// <param name="isDestructable">nem elpsztítható</param>
        /// <param name="isplayer">default false</param>
        public PowerUps(Bonus extra, Point helyzet, string skin, bool isDestructable, bool isplayer = false)
            : base(helyzet, skin, isDestructable, isplayer)
        {
            this.extra = extra;
        }

        /// <summary>
        /// Gets extra
        /// </summary>
        public Bonus Extra
        {
            get
            {
                return this.extra;
            }
        }
    }
}
